##How to use the software and run the program
**Purpose of the program**

The purpose of this application is to calculate the Present Value using 3 parameters:
1) Future Value
2) Interest Rate
3) Number of Years

With regards to number of years, that can be further divided into annual (divisible by 1), semi-annually (divisible by 2), monthly (divisible by 12), or daily (divisible by 365). 
This calculator allows anyone who is thinking of investing their money, to determine their Future Value and determine, how much needs to be invested today for that to take place.
Thanks!

---
##How to use the software and run the program

**Steps Required to Run the Program**  
	a) Download the entire file found on BitBucket  
	b) Save the files into one folder on your desktop  
	c) Ensure that you have Microsoft Visual Studio, downloaded and installed onto your desktop  
		i) If you do not currently have Microsoft Visual Studio, download the community version found in the link below  
			**Link:** https://visualstudio.microsoft.com/downloads/  
	d) Once you have the files saved onto your desktop, click and open the *PresentValueCalculator.sln*  
	e) Once it is completely opened, click *Ctr + F5* to run the application, to avoid debugging on the application  
	f) Once it is completed with running the application, play around and test the Present Value, you need to incur the FV.  